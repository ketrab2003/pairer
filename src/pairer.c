#include <stdio.h>
#include <stdlib.h>
#include <time.h>   // time

int main(){
    srand(time(NULL));

    FILE *fin = fopen("dane.txt", "r");
    FILE *fout = fopen("pary.txt", "w");

    // count numbers in file
    int numcount = 0;
    for (char c = getc(fin); c != EOF; c = getc(fin))
        if (c == '\n')
            numcount += 1;

    // load numbers to memory
    rewind(fin);
    int *nums = malloc(numcount*sizeof(int));
    for(int i=0; i<numcount; ++i)
        fscanf(fin, "%d", nums+i);

    for(int i=0; i<numcount; ++i){
        if(nums[i] < 0)
            continue;

        // find free pair
        int j;
        do{
            j = rand() % (numcount);
        } while(nums[j] < 0 || j == i);

        fprintf(fout, "%d - %d\n", nums[i], nums[j]);
        nums[i] = -1;
        nums[j] = -1;
    }

    fclose(fin);
    fclose(fout);
    return 0;
}
