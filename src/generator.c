#include <stdio.h>
#include <stdlib.h>
#include <time.h>   // time

int main(){
    srand(time(NULL));

    FILE* f = fopen("dane.txt", "w");

    int n;
    printf("Podaj liczbe par (n/2): ");
    scanf("%d", &n);
    n *= 2;

    int *used = malloc(n*sizeof(int));
    for(int i=0; i<n; ++i)
        used[i] = -1;

    int new_num;
    for(int i=0; i<n; ++i){
        new_num = rand() % (n*100);

        // check if number was not randed previously
        char valid = 1;
        for(int j=0; j<i; ++j){
            if(used[j] == new_num){
                valid = 0;
                break;
            }
        }
        if(!valid)
            continue;

        fprintf(f, "%d\n", new_num);
        used[i] = new_num;
    }

    fclose(f);
    return 0;
}
