# Pairer
Two programs to generate list of random numbers and pair them randomly.
## Getting started
 - Execute ```make``` command in project's main directory to compile both programs
 - Programs will be in ```build``` directory
### Example compilation and execution on Windows:
```
$ make
$ cd build
$ generator.exe
$ pairer.exe
```
### Example compilation and execution on Linux:
```
$ make
$ cd build
$ ./generator
$ ./pairer
```
 - Random list of numbers will be in ```dane.txt``` file
 - Paired numbers will be in ```pary.txt``` file
## Authors
 - Bartek K. - [ketrab2003](https://gitlab.com/ketrab2003)