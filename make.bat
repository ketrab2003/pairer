@echo off

if exist .\build\generator.exe del .\build\generator.exe
if exist .\build\pairer.exe del .\build\pairer.exe

if NOT "%1" == "remove" (
    if NOT exist .\build mkdir build

    gcc %~dp0src\generator.c -o %~dp0build\generator.exe
    gcc %~dp0src\pairer.c -o %~dp0build\pairer.exe
)