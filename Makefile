build: generator pairer
generator:
	gcc ./src/generator.c -o ./build/generator
pairer:
	gcc ./src/pairer.c -o ./build/pairer

remove:
	rm ./build/generator ./build/pairer

remake: remove all